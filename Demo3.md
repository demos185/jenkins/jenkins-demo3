## Demo Project:
Create a Jenkins Shared Library
## Technologies used:
Jenkins, Groovy, Docker, Git, Java, Maven
## Project Decription:
Create a Jenkins Shared Library to extract cmmon obuild logic:
* Create separate Git repository for Jenkins Shared Library project
* Create functions in the JSL to use in the Jenkins pipeline
* Integrate and use the JSL in Jenkins Pipeline (globally and for a specific project in Jenkinsfile)
## Description in details:
### Create separate Git repository for Jenkins Shared Library project
__Step 1:__  create new git repository
### Create functions in the JSL to use in the Jenkins pipeline 
__Step 1:__ create folder `vars` for each function/execution and write groovy code for it(e.g. build image, buildjar etc.)

_Note: Each function/execution step is its own Groovy file_

__Step 2:__ Push it in Your git repository
### Integrate and use the JSL in Jenkins Pipeline (globally and for a specific project in Jenkinsfile)
#### Use the Shared Library in Jenkinsfile to extend the Pipeline
__Step 1:__ rerutn to [java-maven](https://gitlab.com/demos185/jenkins/jenkins-demo1-2/-/tree/Jenkins-job) app and use/create new branch that called `jenkins-share-library`

#### Make  the Shared Library available:
>#### Globaly
>__Step 1:__ Open Jenkins UI and go to `Manage Jenkins/Configure system`, open `Global Pipeline Libraries` and fulfill that form
>* Name
>* Default version
>
>__Note:__ Fill `Default version` with fixed version than branch(e.g. master) because every new change  will be immediatly available in whole jenkins for all pipelines and it's breaking change, then your Jenkins file or your pipelines will not work naymore
>
>* Turn on Retrieval method and assign your git repository
>
> __Step 2:__ Return to your new branch and open `Jenkinsfile`
> 
> __Step 3:__ Import Global Library
>```groovy
>@Librarty('Name_of_defined_global_library')
>```
> _Note: If Youd don't have an input statement or a variable definition add __underscore__  in the end of line to avoid syntax errors:_
>```groovy
>@Librarty('Name_of_defined_global_library')_
>```
> __Step 4:__ Push changes to git

>#### For specific Pipeline
>__Step 1:__ Open your `Jenkinsfile` and eddit it
>```groovy 
>library identifier: 'jenkins-shared-library@master', retriever: modernSCM()
>        [$class: 'GitSCMSource',
>        remote: 'URL of your shared library'
>        credentialsId: 'your credentials_id'
>        ]
>```
> __Step 2:__ push changes to git
>  
