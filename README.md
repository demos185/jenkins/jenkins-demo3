## List of Jenkins Demos
1. Install Jenkins on DigitalOcean
2. Createa CI Pipeline with Jenkinsfile (Freestyle, Pipeline, Multibranch Pipeline)
3. Create a Jenkins Shared Library
4. Configure Webhook to trigger CI Pipeline automatically on every change
5. Dynamically Increment Application version in Jenkins Pipeline
